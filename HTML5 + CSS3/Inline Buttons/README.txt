A collection of inline buttons to help get CSS3 projects off the ground. Built with SASS/HAML.
The CSS3 output does not include hover/active styles by default, but they are present in the scss source.

Created by Dillon Sylvestre.