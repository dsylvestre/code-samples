How to give an element a viewport relative height using the CSS3 vh unit. Works anywhere regardless of parent container.

Created by Dillon Sylvestre.