CREATE TABLE `tekriderblog_categories` (
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) default NULL,
	`url_name` varchar(255) default NULL,
	`description` text,
	`created_at` datetime default NULL,
	`updated_at` datetime default NULL,
	`created_user_id` int(11) default NULL,
	`updated_user_id` int(11) default NULL,
	PRIMARY KEY  (`id`),
	KEY `url_name` (`url_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

alter table tekriderblog_categories add column code varchar(50);

CREATE TABLE `tekriderblog_posts_categories` (
  `category_id` int(11) NOT NULL default '0',
  `post_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`category_id`,`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

insert into tekriderblog_posts_categories(post_id, category_id) select id, category_id from tekriderblog_posts;