CREATE TABLE `tekriderblog_posts` (
	`id` int(11) NOT NULL auto_increment,
	`created_at` datetime default NULL,
	`updated_at` datetime default NULL,
	`created_user_id` int(11) default NULL,
	`updated_user_id` int(11) default NULL,
	`title` varchar(255) default NULL,
	`description` text,
	`content` text,
	`published_date` datetime default NULL,
	`is_published` tinyint(4) default NULL,
	`category_id` int(11) default NULL,
	`url_title` varchar(255) default NULL,
	`comments_allowed` tinyint(4) default NULL,
	PRIMARY KEY  (`id`),
	KEY `url_title` (`url_title`),
	KEY `category_id` (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;