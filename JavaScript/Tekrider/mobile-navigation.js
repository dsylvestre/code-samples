// .
// . Mobile navigation menu
// .

$(document).ready(function() {

  $('#mobile_menu_toggle').click(function() {

    e = $('#top_nav ul');

    if ($(e).is(':visible')) {
      $(e).velocity("slideUp", function() {
        duration: 1500
      });
    } else {
      $(e).velocity("slideDown", function() {
        duration: 1500
      });
    }

  });

  /* Prevent menu from hiding when viewport enlarges from mobile state */
  mainNav = $("#top_nav ul");
  $(window).smartresize(function() {
    if ($(window).width() > 480) {
      resetStyleCss(mainNav);
    } else {}
  });

  // Reset jQuery CSS changes
  function resetStyleCss(element) {
    element.removeAttr('style');
  }

});