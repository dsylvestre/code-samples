// .
// . Zoom jQuery plugin configuration
// . http://www.jacklmoore.com/zoom
// .

// . Product page
// ------------------------------------------------------ x

//  Product page image zoom init. function
function init_productZoom() {

  // dont load if browser is IE
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  // get window size
  var windowsize = $(window).width();

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    // .. do not load plugin for IE
  } else {
    $('.product-image').zoom({
      on: 'mouseover',
      url: $(this.img).src,
      callback: function() {
        // disable zoom on mobile
        if (windowsize < 769) {
          $(this).trigger('zoom.destroy'); // remove zoom
        }
      }
    });

  }

}

// Enable zoom plugin based on window size
$(window).smartresize(function() {

  windowsize = $(window).width();

  if (windowsize < 769) {
    $('.product-image').trigger('zoom.destroy'); // remove zoom
  } else {
    $('.product-image').zoom(); // add zoom
  }

});