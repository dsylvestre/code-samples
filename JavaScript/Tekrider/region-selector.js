// .
// . Region selector javascript
// . Used by `regionselect` partial
// .

$(document).ready(function() {

  // setup default shown option
  var a = $('.region-selector li.active').html();
  $(a).prependTo('.active-option');

  // dropdown visibility toggle
  $('.active-option').click(function() {
    $('.region-selector ul.region-options').slideToggle();
  });

  // update URL on option click
  $('.region-options li').click(function() {

    a = $(this).attr('domain'); // 'ca' or 'us'

    // show yes or no prompt
    swal({
        title: "Are you sure?",
        text: "You will lose any items you have added to your cart.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "Go back",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {

          $new_url = window.location.origin + "/" + a.toLowerCase() + "/";

          // check if remote
          if ((window.location.pathname).indexOf("~tekrider") != false) {
            $new_url = window.location.origin + "/" + a.toLowerCase() + "/";
          }

          window.location.href = $new_url;

        } else {
          //swal("Phew", "That was close :)", "error");
          // slide dropdown back up
          $('.region-selector ul.region-options').slideToggle();
        }
      });

  });

});