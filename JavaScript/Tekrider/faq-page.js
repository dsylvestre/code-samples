// .
// . FAQ page Javascript
// .

$(window).load(function() {

  // . Toggle display of FAQ answer
  // ------------------------------------------------------ x

  $("#faqs dt").click(function() {

    // FAQ question clicked ..

    e = $(this).next("dd"); // faq answer element

    if ($(e).is(':visible')) {

      $(e).velocity("slideUp", function() {
        duration: 300
      });

      // invert icon class
      $(".faq-icon", this).toggleClass("icon-uniF476");
      $(".faq-icon", this).toggleClass("icon-uniF477");

    } else {

      $(e).velocity("slideDown", function() {
        duration: 300
      });

      // invert icon class
      $(".faq-icon", this).toggleClass("icon-uniF476");
      $(".faq-icon", this).toggleClass("icon-uniF477");

    }

  });

});