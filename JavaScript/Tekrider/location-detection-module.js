// .
// . Location detection module cookie javascript
// .
// . Saves information about users location to cookie.
// .

function assignCountryCookie() {

  // Create & update country cookie when flag image is clicked

  var countryCookieDays = 14; // Number of days until cookie expires

  // CANADA
  $("#header_flag_canada").click(function() {
    createCookie('TekriderUserCountry', 'canada', countryCookieDays);
    createCookie('TekriderUserGroupID', 5, countryCookieDays);
    alert("Changed country to: Canada");
    location.reload();
  });

  // USA
  $("#header_flag_us").click(function() {
    createCookie('TekriderUserCountry', 'united states', countryCookieDays);
    createCookie('TekriderUserGroupID', 7, countryCookieDays);
    alert("Changed country to: United States");
    location.reload();
  });

  // Cookie create / read / delete functions
  // @ http://www.quirksmode.org/js/cookies.html

  function createCookie(name, value, days) {
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
  }

  function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }

  function eraseCookie(name) {
    createCookie(name, "", -1);
  }

}