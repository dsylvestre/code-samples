// .
// . Product extra options calculator
// .

/*
  This is used to update the product price displayed in real-time.
  The CMS LemonStand will not do this by default, although these prices are sent sent to the cart correctly.
  Required for customizable products: The OutBack.
*/

// required by update_promo_product_price() function
function get_extra_option_price(option) {
  var price = jQuery('input.extra_option_price', $(option).parents('.extra-option label')).val();
  // console.log("Checked option price: "+price);
  if (!isNaN(price))
    return parseFloat(price);
  return 0;
}

function update_promo_product_price() {
  var extra_price = 0;

  $(".extra-option input:checked").each(function() {
    extra_price += get_extra_option_price(this);
    //console.log("Checked extra option: "+this);
  });

  // this element can be found in the promo product price partial
  var updated_price = parseFloat($("#base_product_price").val()) + extra_price;

  // console.log("Updated price: "+updated_price);

  $('#partial-price-1').text('$' + updated_price.formatNumber(2, '.', ''));

}

function init_price_calculator() {
  // this element corresponds to the extra option checkboxes
  $(".extra-option label").click(update_promo_product_price);
  update_promo_product_price();
}

/* Extend the Number class with the formatNumber method for formatting
   currency values (from ls simplicity theme)
------------------------------------------------------------------------ */

Number.prototype.formatNumber = function(c, d, t) {
  var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "," : d,
    t = t == undefined ? "." : t,
    s = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;

  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}