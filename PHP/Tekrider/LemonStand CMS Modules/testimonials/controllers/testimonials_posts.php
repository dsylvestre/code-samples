<?

	class Testimonials_Posts extends Backend_Controller
	{
		public $implement = 'Db_ListBehavior, Db_FilterBehavior, Db_FormBehavior';
		public $list_model_class = 'Testimonials_Post';
		public $list_record_url = null;
		public $list_search_enabled = true;
		public $list_search_fields = array('@title', '@description', '@content', '@author');
		public $list_search_prompt = 'find posts by title or content';
		public $form_model_class = 'Testimonials_Post';
		public $form_redirect = null;

		public $form_create_title = 'New Post';
		public $form_edit_title = 'Edit Post';
		public $form_not_found_message = 'Post not found';

		public function __construct()
		{
			parent::__construct();
			$this->app_module_name = 'Testimonials';
			$this->app_tab = 'testimonials';
			$this->app_page = 'posts';

			$this->list_record_url = url('/testimonials/posts/edit/');
			$this->form_redirect = url('/testimonials/posts');
		}

		public function index()
		{
			$this->app_page_title = 'Manage testimonials';
		}
	}

?>