<?php

	class Testimonials_Post extends Db_ActiveRecord
	{
		public $table_name = 'testimonials_posts';
		public $implement = 'Db_AutoFootprints';
		public $auto_footprints_visible = true;

		// Has many relationship binding this classes table to the image db table
		public $has_many = array(

			'images' => array(
				'class_name' => 'Db_File',
				'foreign_key' => 'master_object_id',
				'conditions' => "master_object_class='Testimonials_Post' and field='images'",
				'order' => 'sort_order, id',
				'delete' => true
			)
		);
		public

		function define_columns($context = null)
		{
			$this->define_column('id', '#');

			// Content item image array
			$this->define_multi_relation_column('images', 'images', 'Images', '@name')->invisible();
			$this->define_column('author', 'Author')->validation()->fn('trim')->required('Please specify a testimonial author');
			$this->define_column('author_description', 'Author description');
			$this->define_column('author_link', 'Author link')->invisible();
			$this->define_column('testimonial_date', 'Date of testimony');
			$this->define_column('content', 'Content')->invisible();
			$this->define_column('is_featured', 'Main page feature');
			$this->define_column('is_disabled', 'Disabled');
			$this->define_column('title', 'Title')->invisible();
			$this->define_column('description', 'Testimonial excerpt')->invisible();
		}

		public

		function define_form_fields($context = null)
		{
			$this->add_form_field('author')->tab('Testimonial information');
			$this->add_form_field('author_description')->tab('Testimonial information');
			$this->add_form_field('author_link')->tab('Testimonial information');
			$this->add_form_field('testimonial_date')->tab('Testimonial information');
			$this->add_form_field('description')->tab('Testimonial information');
			$this->add_form_field('content')->tab('Testimonial content')->renderAs(frm_html);
			$this->add_form_field('is_featured')->tab('Testimonial information')->renderAs(frm_checkbox);
			$this->add_form_field('is_disabled')->tab('Testimonial information')->renderAs(frm_checkbox);

			// Images
			$this->add_form_field('images')->renderAs(frm_file_attachments)->renderFilesAs('image_list')->addDocumentLabel('Add image(s)')->tab('Testimonial Images')->noAttachmentsLabel('There are no images uploaded')->noLabel()->imageThumbSize(555)->fileDownloadBaseUrl(url('ls_backend/files/get/'));
		}
	}

?>
