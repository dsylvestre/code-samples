<?php

	class Testimonials_Actions extends Cms_ActionScope
	{

		function posts()
		{
			$obj = new Testimonials_Post();
			$this->data['posts'] = $obj->order('created_at desc')->find_all();
		}

		function post()
		{
			$this->data['post'] = null;
			$post_id = $this->request_param(0);
			if (!strlen($post_id)) return;
			$obj = new Testimonials_Post();
			$this->data['post'] = $obj->find($post_id);
		}

	}

?>