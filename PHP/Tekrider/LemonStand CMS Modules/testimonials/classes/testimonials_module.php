<?php

	class Testimonials_Module extends Core_ModuleBase
	{
		/**
		 * Creates the module information object
		 * @return Core_ModuleInfo
		 */
		protected function createModuleInfo()
		{
			return new Core_ModuleInfo(
				"Tekrider Testimonials",
		        "Adds testimonial feature to site",
		        "Dillon Sylvestre"
		    );
		}

		/**
		 * Returns a list of the module back-end GUI tabs.
		 * @param Backend_TabCollection $tabCollection A tab collection object to populate.
		 * @return mixed
		 */
		public function listTabs($tabCollection)
		{
			$menu_item = $tabCollection->tab('testimonials', 'Testimonials', 'posts', 90);
			$menu_item->addSecondLevel('posts', 'Manage testimonials', 'posts');
		}
	}

?>