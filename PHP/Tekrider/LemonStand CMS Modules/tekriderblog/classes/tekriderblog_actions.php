<?php

	class TekriderBlog_Actions extends Cms_ActionScope
	{
		public

		function posts()
		{
			$obj = new TekriderBlog_Post();
			$this->data['posts'] = $obj->order('created_at desc')->find_all();
		}

		public

		function post()
		{
			$this->data['post'] = null;
			$post_id = $this->request_param(0);
			if (!strlen($post_id)) return;
			$obj = new TekriderBlog_Post();
			$this->data['post'] = $obj->find($post_id);
			if (post('add_comment')) $this->on_addComment();
		}

		public

		function category()
		{
			$this->data['category'] = null;
			$url_name = $this->request_param(0);
			if (!strlen($url_name)) return;
			$category = TekriderBlog_Category::create()->find_by_url_name($url_name);
			if (!$category) return;
			$this->data['category'] = $category;
			$this->data['posts'] = $category->posts_list;
		}

		public

		function on_addComment()
		{
			/*
			* Some code for adding a comment
			*/
			/*
			* Trigger some error
			*/
			throw new Phpr_ApplicationException('Error adding the comment to the database!');
		}
	}

?>