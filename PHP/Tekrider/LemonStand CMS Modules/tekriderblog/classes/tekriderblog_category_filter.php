<?

	class TekriderBlog_Category_Filter extends Db_DataFilter
	{
		public $model_class_name = 'TekriderBlog_Category';
		public $list_columns = array('name');

		public function applyToModel($model, $keys, $context = null)
		{
			$model->where('(select count(*) from tekriderblog_posts_categories where category_id in (?) and post_id=tekriderblog_posts.id) > 0', array($keys));
		}
	}

?>
