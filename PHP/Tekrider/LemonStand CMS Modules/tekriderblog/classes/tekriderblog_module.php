<?php

	class TekriderBlog_Module extends Core_ModuleBase
	{
		/**
		 * Creates the module information object
		 * @return Core_ModuleInfo
		 */
		protected function createModuleInfo()
		{
			return new Core_ModuleInfo(
				"Tekrider Blog",
				"Adds news/media blogging features to site",
				"Dillon Sylvestre" );
		}

		/**
		 * Returns a list of the module back-end GUI tabs.
		 * @param Backend_TabCollection $tabCollection A tab collection object to populate.
		 * @return mixed
		 */
		public function listTabs($tabCollection)
		{
			$menu_item = $tabCollection->tab('tekriderblog', 'Tekrider Media', 'posts', 90);
			$menu_item->addSecondLevel('posts', 'Media posts', 'posts');
			$menu_item->addSecondLevel('categories', 'Media categories', 'categories');
		}
	}

?>
