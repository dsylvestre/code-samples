<?

	class TekriderBlog_Posts extends Backend_Controller
	{
		public $implement = 'Db_ListBehavior, Db_FilterBehavior, Db_FormBehavior';
		public $list_model_class = 'TekriderBlog_Post';
		public $list_record_url = null;
		public $list_search_enabled = true;
		public $list_search_fields = array('@title', '@description', '@content');
		public $list_search_prompt = 'find posts by title or content';

		public $list_options = array();
		public $list_custom_prepare_func = null;

		public $filter_filters = array(
			'category'=>array('name'=>'Category', 'class_name'=>'TekriderBlog_Category_Filter', 'prompt'=>'Please choose post categories you want to include to the list. Posts with other categories will be hidden.', 'added_list_title'=>'Added Categories')
		);
		public $filter_list_title = 'Filter posts';
		public $filter_onApply = 'listReload();';
		public $filter_onRemove = 'listReload();';

		public $form_create_title = 'New Post';
		public $form_edit_title = 'Edit Post';
		public $form_model_class = 'TekriderBlog_Post';
		public $form_not_found_message = 'Post not found';
		public $form_redirect = null;

		public function __construct()
		{
			parent::__construct();
			$this->app_module_name = 'Tekrider Blog';
			$this->app_tab = 'tekriderblog';
			$this->app_page = 'posts';

			$this->list_record_url = url('/tekriderblog/posts/edit/');
			$this->form_redirect = url('/tekriderblog/posts');
		}

		public function index()
		{
			$this->app_page_title = 'Posts';
		}

		public function listPrepareData()
		{
			$obj = new TekriderBlog_Post();
			$this->filterApplyToModel($obj);

			return $obj;
		}
	}

?>
