<?
	class TekriderBlog_Post extends Db_ActiveRecord
	{
		public $table_name = 'tekriderblog_posts';

		public $implement = 'Db_AutoFootprints';
		public $auto_footprints_visible = true;

		public $has_and_belongs_to_many = array(
			'categories'=>array('class_name'=>'TekriderBlog_Category', 'join_table'=>'tekriderblog_posts_categories', 'primary_key'=>'post_id', 'foreign_key'=>'category_id')
		);

		public function define_columns($context = null)
		{
			$this->define_column('id', '#');
			$this->define_column('title', 'Title')->validation()->fn('trim')->required('Please specify a blog post title')->minLength(4, 'The post title should be at least 4 characters in length');
			$this->define_column('description', 'Description');

			$this->define_multi_relation_column('categories', 'categories', 'Categories', "@name")->validation()->required('Please select at least one category');
			$this->define_column('content', 'Content')->invisible();
		}

		public function define_form_fields($context = null)
		{
			$this->add_form_field('title')->tab('General parameters');
			$this->add_form_field('description')->tab('General parameters');
			$this->add_form_field('categories')->tab('General parameters');
			$this->add_form_field('content')->tab('Content')->renderAs(frm_html);
		}
	}
?>
