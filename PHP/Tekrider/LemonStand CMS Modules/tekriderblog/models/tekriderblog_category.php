<?
	class TekriderBlog_Category extends Db_ActiveRecord
	{
		public $table_name = 'tekriderblog_categories';

		public $implement = 'Db_AutoFootprints';
		public $auto_footprints_visible = true;
		protected $api_added_columns = array();

		// Relate category table categories to posts via ID matching
		public $has_and_belongs_to_many = array(
			'all_posts'=>array('class_name'=>'TekriderBlog_Post', 'join_table'=>'tekriderblog_posts_categories', 'order'=>'created_at desc'),
			'posts'=>array('class_name'=>'TekriderBlog_Post', 'join_table'=>'tekriderblog_posts_categories', 'order'=>'tekriderblog_posts.published_date desc', 'conditions'=>'(is_published is not null and is_published=1)')
		);

		public $calculated_columns = array(
			'post_num'=>array('sql'=>"(select count(*) from tekriderblog_posts, tekriderblog_posts_categories where tekriderblog_posts_categories.category_id=tekriderblog_categories.id and tekriderblog_posts_categories.post_id=tekriderblog_posts.id)", 'type'=>db_number),
			'published_post_num'=>array('sql'=>"(select count(*) from tekriderblog_posts, tekriderblog_posts_categories where tekriderblog_posts_categories.category_id=tekriderblog_categories.id and tekriderblog_posts_categories.post_id=tekriderblog_posts.id and tekriderblog_posts.is_published is not null and tekriderblog_posts.is_published=1)", 'type'=>db_number)
		);

		public static function create()
		{
			return new self();
		}

		// Tekrider blog category user interface definitions
		public function define_columns($context = null)
		{
			$this->define_column('name', 'Name')->order('asc')->validation()->fn('trim')->required("Please specify the category name.");
			$this->define_column('url_name', 'URL Name')->validation()->fn('trim')->fn('mb_strtolower')->regexp('/^[0-9a-z_-]*$/i', 'URL Name can contain only latin characters, numbers, underscores and the minus sign')->required('Please specify the URL Name')->unique('The URL Name "%s" already in use. Please enter another URL Name.');
			$this->define_column('description', 'Description')->validation()->fn('trim');
			$this->define_column('post_num', 'Post Number');

			$this->define_column('code', 'API Code')->defaultInvisible()->validation()->fn('trim')->fn('mb_strtolower')->unique('Category with the specified  API code already exists.');

			$this->defined_column_list = array();
			Backend::$events->fireEvent('blog:onExtendCategoryModel', $this, $context);
			$this->api_added_columns = array_keys($this->defined_column_list);
		}

		public function define_form_fields($context = null)
		{
			$this->add_form_field('name', 'left');
			$this->add_form_field('url_name', 'right');
			$this->add_form_field('description')->renderAs(frm_textarea)->size('small');
			$this->add_form_field('code')->comment('You can use the API Code for accessing the category in the API calls.', 'above');

			Backend::$events->fireEvent('blog:onExtendCategoryForm', $this, $context);
			foreach ($this->api_added_columns as $column_name)
			{
				$form_field = $this->find_form_field($column_name);
				if ($form_field)
					$form_field->optionsMethod('get_added_field_options');
			}
		}
	}
?>
