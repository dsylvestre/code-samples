/* File: gulpfile.js */

// Grab required NPM packages
var gulp 		 = require('gulp'),
    sass         = require('gulp-ruby-sass'),
    rename       = require('gulp-rename')
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    copy         = require('gulp-copy'),
    imageop      = require('gulp-image-optimization');

// . SASS I/O
// ------------------------------------------------------ x

// Preprocess main SASS into CSS, run copy_css_main task
gulp.task('sass_build', function() {

  return sass('source/scss/**/*.scss',
    {
      style: 'compact',
      sourcemap: true,
      compass: false
    }
  )
  .pipe(autoprefixer("last 1 version", "> 1%", "ie 8", "ie 7"))
  .pipe(sourcemaps.write())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('./build/development/css/'));

});

// Preprocess main SASS into CSS, run copy_css_main task (minified for production)
gulp.task('sass_build_prod', function() {

  return sass('source/scss/**/*.scss',
    {
      style: 'compressed',
      sourcemap: false,
      compass: false
    }
  )
  .pipe(autoprefixer("last 1 version", "> 1%", "ie 8", "ie 7"))
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('./build/production/css'));

});

// . Watch files for changes
// ------------------------------------------------------ x

// GulpJS watch task
gulp.task('watch', function() {

	// Watch .scss files
  gulp.watch('./source/scss/**/**/**/*.*', ['sass_build', 'copy_css']);

});

// . Image optimization
// ------------------------------------------------------ x

// Gulp task to optimize images
gulp.task('image_op', function(cb) {

  gulp.src(['./source/img/**/**/**/*.png','./source/img/**/**/**/*.jpg','./source/img/**/**/**/*.gif','./source/img/**/**/**/*.jpeg']).pipe(imageop({
      optimizationLevel: 5,
      progressive: true,
      interlaced: true
  })).pipe(gulp.dest('./source/img')).on('end', cb).on('error', cb);

});

// . Copy;move files
// ------------------------------------------------------ x

// Copies form layout CSS produced by SASS into lemonstand CSS resource dir
gulp.task('copy_css', ['sass_build'], function(){

  src_path = './build/development/css/*.css';
  dest_path_ca = '../www/ca/resources/css/';
  dest_path_us = '../www/us/resources/css/';

  gulp.src([ src_path ])
      .pipe(gulp.dest( dest_path_ca ));
  gulp.src([ src_path ])
      .pipe(gulp.dest( dest_path_us ));

});

// Copies form layout CSS produced by SASS into lemonstand CSS resource dir
gulp.task('copy_css_prod', ['sass_build_prod'], function(){

  src_path = './build/production/css/*.css';
  dest_path = '../resources/css/';
  gulp.src(src_path)
  .pipe(gulp.dest(dest_path));

});

// Gulp task to copy optimized images
gulp.task('image_copy', function (cb) {

  copy_src = './source/img/**/**/**/*.*';
  dest_path_ca = '../www/ca/resources/images/';
  dest_path_us = '../www/us/resources/images/';

  gulp.src([ copy_src ])
      .pipe(gulp.dest( dest_path_ca ));
  gulp.src([ copy_src ])
      .pipe(gulp.dest( dest_path_us ));

});

// . Main tasks for command line
// ------------------------------------------------------ x

// GulpJS default task
gulp.task('default', ['sass_build', 'copy_css', 'watch']);

// Production build
gulp.task('production', ['sass_build_prod']);

// GulpJS default task
gulp.task('images', ['image_op', 'image_copy']);